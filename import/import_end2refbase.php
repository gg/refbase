<?php
	// Project:    Web Reference Database (refbase) <http://www.refbase.net>
	// Copyright:  Matthias Steffens <mailto:refbase@extracts.de> and the file's
	//             original author(s).
	//
	//             This code is distributed in the hope that it will be useful,
	//             but WITHOUT ANY WARRANTY. Please see the GNU General Public
	//             License for more details.
	//
	// File:       ./import/import_end2refbase.php
	// Repository: $HeadURL: https://svn.code.sf.net/p/refbase/code/branches/bleeding-edge/import/import_end2refbase.php $
	// Author(s):  Matthias Steffens <mailto:refbase@extracts.de>
	//
	// Created:    01-Dec-09, 08:05
	// Modified:   $Date: 2012-02-29 01:02:18 +0100 (Wed, 29 Feb 2012) $
	//             $Author: msteffens $
	//             $Revision: 1348 $

	// This is an import format file (which must reside within the 'import/' sub-directory of your refbase root directory). It contains a version of the
	// 'importRecords()' function that imports records from 'Endnote tagged'-formatted data, i.e. data that were formatted according to the tagged export
	// format used by the commercial bibliographic package 'Endnote' (http://www.endnote.com).

	// --------------------------------------------------------------------

	// --- BEGIN IMPORT FORMAT ---

	// Import records from Endnote-formatted source data:

	function importRecords($sourceText, $importRecordsRadio, $importRecordNumbersArray)
	{
		// parse Endnote tagged format:
		return endnoteToRefbase($sourceText, $importRecordsRadio, $importRecordNumbersArray); // function 'endnoteToRefbase()' is defined in 'import.inc.php'
	}

	// --- END IMPORT FORMAT ---

	// --------------------------------------------------------------------
?>
